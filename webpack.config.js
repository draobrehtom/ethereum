const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: './app/javascripts/app.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: 'index.html' },
      { from: './app/index1.html', to: 'index1.html' },
      { from: './app/index2.html', to: 'index2.html' },
      { from: './app/ucp.html', to: 'ucp.html' },
      { from: './app/img', to: 'img' },
      { from: './app/css', to: 'css' },
      { from: './app/js', to: 'js' },
      { from: './app/less', to: 'less' },
      { from: './app/vendor', to: 'vendor' },
    ])
  ],
  devServer: {
    host: 'localhost',
    disableHostCheck: true,
    port: 8008
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ],
    loaders: [
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        loaders: [
          'file?name=images/[sha512:hash:base64:7].[ext]',
          'image-webpack?progressive=true&optimizationLevel=7&interlaced=true'
        ]
      },
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  }
}
