// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    development: {
      host: '0.0.0.0',
      port: 8545,
      network_id: '*', // Match any network id
      // from: "0xa4d60308e9903ba7718f614bc863b78457e2af9b"

    },
    live: {
      host: '0.0.0.0', // Random IP for example purposes (do not use)
      port: 80,
      network_id: 1      // Ethereum public network
        // optional config values:
        // gas
        // gasPrice
        // from - default address to use for any transaction Truffle makes during migrations
        // provider - web3 provider instance Truffle should use to talk to the Ethereum network.
        //          - if specified, host and port are ignored.
    }
  }
}
