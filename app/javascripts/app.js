// Import the page's CSS. Webpack will know what to do with it.
//import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

import mytoken_artifacts from '../../build/contracts/MyToken.json'
var MyToken = contract(mytoken_artifacts);

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;



window.App = {

  test: function() {
    var self = this;
    var meta;

    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.getBalance.call(account, {from: account});
    }).then(function(value) {
      console.log(value);
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error");
    })

  },

  deposit: function() {
    var self =  this;
    var meta;
    var amount = parseFloat(document.getElementById("amountToDeposit").value);

    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.deposit(web3.toWei(amount, "ether"), {from: account});
    }).then(function(value) {
      self.depositBalance();
      self.setStatus("Deposit success");
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error deposit to contract ");
    });
  },

  withdrawDeposit: function() {
    var self =  this;
    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.withdrawDeposit(0, {from: account});
    }).then(function(value) {
      self.depositBalance();
      self.refreshBalance();
      self.setStatus("Deposit success");
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error deposit to contract ");
    });
  },

  depositBalance: function() {
    var self =  this;
    var meta;

    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.depositBalance(0, {from: account});
    }).then(function(value) {
      var deposit_balance = document.getElementById("depositBalance");
      deposit_balance.innerHTML = web3.fromWei(value[0].valueOf(), "ether");
      var deposit_status = document.getElementById("depositStatus");
      document.getElementById("withdrawDeposit").disabled = value[1];
      if (value[1]) {
        value[1] = "да";
      } else {
        value[1] = "нет";
      }

      deposit_status.innerHTML = value[1];
      var deposit_percent = document.getElementById("depositPercent");
      deposit_percent.innerHTML = value[2];
      var deposit_startTime = document.getElementById("depositStartTime");
      var date  = (new Date (value[3] * 1000)).toString();
      deposit_startTime.innerHTML = date.substring(0, date.length - 6);
      // TODO: summ time and show
      date  = new Date (value[3] * 1000);
     // date = date.setMonth(date.getMonth()+1);
      var deposit_endTime = document.getElementById("depositEndTime");
      deposit_endTime.innerHTML = new Date (value[3] * 1000 + value[4] * 1000);
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error getting deposit balance ");
    });
  },

  contractBalance: function() {
    var self =  this;
    var meta;

    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.getContractBalance(account, {from: account});
    }).then(function(value) {
      var balance_element = document.getElementById("contractBalance");
      balance_element.innerHTML = web3.fromWei(value.valueOf(), "ether");
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error getting contract balance ");
    });
  },

  buy: function() {
    var self = this;

    //var amount = parseInt(document.getElementById("amountToBuy").value);

    var amount = parseFloat(document.getElementById("amount").value);


    // 1 FRC = 0.5 ether



    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.buy({from:account, value: web3.toWei(amount, "ether")});
    }).then(function(t) {
      console.log(t);
      self.setStatus("Transaction buy complete!");
      self.refreshBalance();
      self.contractBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error buying coin ");
    });
  },

  sell: function() {
    var self = this;
    var amount = parseFloat(document.getElementById("amount").value);
    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.sell(web3.toWei(amount, "ether"), {from:account});
    }).then(function(t) {
      console.log(t);
      self.setStatus("Transaction sell complete!");
      self.refreshBalance();
      self.contractBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error selling coin ");
    });
  },

  start: function() {



    var self = this;

    // Bootstrap the MyToken abstraction for Use.
    MyToken.setProvider(web3.currentProvider);


    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Не удалось загрузить аккаунт Ethereum. Убедитесь, что вы авторизованы в MetaMask.");
        return;
      }

      accounts = accs;
      account = accounts[0];


      var address_element = document.getElementById("address");
      address_element.innerHTML = account;

      self.refreshBalance();
      self.contractBalance();
      self.depositBalance();
      self.getFounders();
    });


  },

  setStatus: function(message, type = "") {
    var status = document.getElementById("status" + type);
    status.innerHTML = message;
  },

  refreshBalance: function() {
    // document.getElementsByClassName('bg-2')[0].style.visibility = 'hidden';
    var self = this;

    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.getBalance({from: account});
    }).then(function(value) {
      var balance_element = document.getElementById("balance");
      balance_element.innerHTML = web3.fromWei(value.valueOf(), "ether");
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error getting balance ");
    });
  },

  sendCoin: function() {
    var self = this;

    var amount = parseFloat(document.getElementById("amount").value);
    var receiver = document.getElementById("receiver").value;

    this.setStatus("Initiating transaction... (please wait)");

    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.transfer(receiver, web3.toWei(amount,"ether"), {from: account});
    }).then(function() {
      self.setStatus("Transaction complete!");
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error sending coin ");
    });
  },

  setPrivateDepositPercent: function() {
    var self = this;

    var address = document.getElementById("address").value;
    var percent = parseFloat(document.getElementById("amount").value);
    // this.setStatus("Initiating transaction... (please wait)");

    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.setPrivateDepositPercent(address, percent, {from: account});
    }).then(function() {
      self.setStatus("Transaction complete!");
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error ");
    });
  },
  setPrivateDepositTime: function() {
    var self = this;

    var address = document.getElementById("address").value;
    var time = parseFloat(document.getElementById("amount").value);
    // this.setStatus("Initiating transaction... (please wait)");

    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.setPrivateDepositTIme(address, time, {from: account});
    }).then(function() {
      self.setStatus("Transaction complete!");
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error ");
    });
  },
  getFounders: function() {
    var self =  this;
    var meta;

    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.getFounders({from: account});
    }).then(function(value) {
       for (var i = 0; i < 4; i++) {
         document.getElementById("acc" + i).innerHTML = value[i];
       }

      // var balance_element = document.getElementById("contractBalance");
      // balance_element.innerHTML = value[0];
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error getting contract founders ", 1);
    });
  },
  setCoFounder: function() {
    var self = this;

    var address = document.getElementById("address1").value;
    var slot = parseFloat(document.getElementById("slot").value);

    if (slot < 1 || slot > 3){
      return;
    }

    this.setStatus("Initiating transaction... (please wait)", 1);

    var meta;
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.setCoFounder(address, slot, {from: account});
    }).then(function() {
      self.setStatus("Transaction complete!", 1);
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error ", 1);
    });
  },

  test1: function () {
    var meta;
    var self = this;
    MyToken.deployed().then(function (instance) {
      meta=instance;
      return meta.name;
    }).then(function (value) {
      console.log(value);
    }).catch(function (e) {
      console.log(e);
      self.setStatus("Ошибки, они везде, и в этот раз не получилось достать счета основателей. Возможно, проблема где-то рядом.")
    })
  },

  setPrice: function () {
    var self = this;
    var meta;
    var price = $("#price").val();
    MyToken.deployed().then(function(instance) {
      meta = instance;
      return meta.setPrice(price, {from: account});
    }).then(function() {
      self.setStatus("Price installed", 1);
    }).catch(function(e) {
      console.log(e);
      self.setStatus("Error setting price", 1);
    });
  }


};

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {

    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();
});
