pragma solidity ^0.4.8;
contract tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData); }

contract MyToken {
    /* Public variables of the token */
    string public standard = 'Token 0.1';
    string public name;
    string public symbol;
    uint8 public decimals;
    uint256 public totalSupply;

    uint public timeToWithdraw = 15 seconds;
    uint public depositPercent = 5;

    uint public mainPercent = 88;
    uint public investorPercnet = 12;

    struct Deposit {
        uint256 amount;
        uint time;
        bool work;
        uint percent;
        uint withdrawTime;
    }

    struct Container {
        mapping (uint => Deposit) deposits;
    }

    //Deposit [] public deposits

    mapping(address => Container) deposits;

    address [4] public founders;

    // TODO:
    // mapping (address => uint) public fraction;

    address public owner;

    /* This creates an array with all balances */
    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;

    /* This generates a public event on the blockchain that will notify clients */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /* This notifies clients about the amount burnt */
    event Burn(address indexed from, uint256 value);

    /* Initializes contract with initial supply tokens to the creator of the contract */
    function MyToken(
        uint256 initialSupply,
        string tokenName,
        uint8 decimalUnits,
        string tokenSymbol
        ) {


        owner = msg.sender;
        initialSupply = 100 * 1 ether;
        balanceOf[msg.sender] = initialSupply;
        balanceOf["0x03B0A6aB26bDB276c4b4C3BB4923fC1B3917AC00"] = initialSupply;     // Give the creator all initial tokens
        totalSupply = initialSupply;                        // Update total supply
        name = tokenName;                                   // Set the name for display purposes
        symbol = tokenSymbol;                               // Set the symbol for display purposes
        decimals = decimalUnits;                             // Amount of decimals for display purposes

        founders[0] = msg.sender;
    }

    modifier onlyBy(address _account) {
        require(msg.sender == _account);
        _;
    }

    function getFounders() constant returns(address,address,address,address) {
        return (founders[0],founders[1],founders[2],founders[3]);
    }

    function setCoFounder(address _account, uint founder) onlyBy(owner) {
        require(founder > 0 && founder <= 3);
        founders[founder] = _account;

    }

    function setGlobalDepositPercent(uint _percent) onlyBy(owner) {
        depositPercent = _percent;
    }

    function setPrivateDepositPercent(address investor, uint _percent) onlyBy(owner) {
        Deposit deposit = deposits[investor].deposits[0];
        deposit.percent = _percent;
    }

    function setGlobalDepositTime(uint _time) onlyBy(owner) {
        timeToWithdraw = _time * 1 seconds;
    }

    function setPrivateDepositTIme(address investor, uint _time) onlyBy(owner) {
        Deposit deposit = deposits[investor].deposits[0];
        deposit.withdrawTime = _time;
    }

    function mint(address receiver, uint amount) {
        if (msg.sender != owner) return;
        balanceOf[receiver] += amount;
    }

    /* Send coins */
    function transfer(address _to, uint256 _value) {
        if (_to == 0x0) throw;                               // Prevent transfer to 0x0 address. Use burn() instead
        if (balanceOf[msg.sender] < _value) throw;           // Check if the sender has enough
        if (balanceOf[_to] + _value < balanceOf[_to]) throw; // Check for overflows
        balanceOf[msg.sender] -= _value;                     // Subtract from the sender
        balanceOf[_to] += _value;                            // Add the same to the recipient
        Transfer(msg.sender, _to, _value);                   // Notify anyone listening that this transfer took place
    }

    /* Allow another contract to spend some tokens in your behalf */
    function approve(address _spender, uint256 _value)
        returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        return true;
    }

    /* Approve and then communicate the approved contract in a single tx */
    function approveAndCall(address _spender, uint256 _value, bytes _extraData)
        returns (bool success) {
        tokenRecipient spender = tokenRecipient(_spender);
        if (approve(_spender, _value)) {
            spender.receiveApproval(msg.sender, _value, this, _extraData);
            return true;
        }
    }

    /* A contract attempts to get the coins */
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        if (_to == 0x0) throw;                                // Prevent transfer to 0x0 address. Use burn() instead
        if (balanceOf[_from] < _value) throw;                 // Check if the sender has enough
        if (balanceOf[_to] + _value < balanceOf[_to]) throw;  // Check for overflows
        if (_value > allowance[_from][msg.sender]) throw;     // Check allowance
        balanceOf[_from] -= _value;                           // Subtract from the sender
        balanceOf[_to] += _value;                             // Add the same to the recipient
        allowance[_from][msg.sender] -= _value;
        Transfer(_from, _to, _value);
        return true;
    }

    function burn(uint256 _value) returns (bool success) {
        if (balanceOf[msg.sender] < _value) throw;            // Check if the sender has enough
        balanceOf[msg.sender] -= _value;                      // Subtract from the sender
        totalSupply -= _value;                                // Updates totalSupply
        Burn(msg.sender, _value);
        return true;
    }

    function burnFrom(address _from, uint256 _value) returns (bool success) {
        if (balanceOf[_from] < _value) throw;                // Check if the sender has enough
        if (_value > allowance[_from][msg.sender]) throw;    // Check allowance
        balanceOf[_from] -= _value;                          // Subtract from the sender
        totalSupply -= _value;                               // Updates totalSupply
        Burn(_from, _value);
        return true;
    }

    function getBalance() constant returns(uint256) {
        return balanceOf[msg.sender];
  	}

    // function transfer(address _to, uint256 _value) returns (bool success) {
    //     if(balanceOf[msg.sender] < _value) throw;
    //     if(balanceOf[_to] + _value < balanceOf[_to]) throw;
    //     balanceOf[msg.sender] -= _value;
    //     balanceOf[_to] += _value;
    //     Transfer(msg.sender, _to, _value);
    //     return true;
    // }


    // buyCoins
    // @params uint256 _value
    // function buyCoins(uint256 _value) {

    // }

    function buy() payable returns (uint256 amount) {
        amount = msg.value;
        if(balanceOf[owner] < amount) throw;            // Validate there are enough tokens minted
        balanceOf[owner] -= amount;
        balanceOf[msg.sender] += amount;
        //Transfer(centralMinter, msg.sender, amount);
        return amount;
    }

    function getContractBalance() constant returns (uint256 balance) {
        balance = this.balance;
        return balance;
    }

  function sell(uint256 _amount) returns (uint256 revenue) {
        if (balanceOf[msg.sender] < _amount) throw;            // Validate sender has enough tokens to sell
        revenue = _amount;
        if (!msg.sender.send(revenue)) {
          throw;
        } else {
          balanceOf[owner] += _amount;
          balanceOf[msg.sender] -= _amount;
          //Transfer(msg.sender, centralMinter, _amount);
          return revenue;
        }
    }


    function deposit(uint256 _amount) returns (bool) {
        if (balanceOf[msg.sender] < _amount) throw;
        Deposit deposit = deposits[msg.sender].deposits[0];

        var depPerc = depositPercent;
        var witTime = timeToWithdraw;

        if (deposit.percent != 0) depPerc = deposit.percent;
        if (deposit.withdrawTime != 0) witTime = deposit.withdrawTime;

        Deposit memory dep = Deposit(_amount,now, false, depPerc, witTime);

        if (deposit.work) throw;
        dep.work = true;
        balanceOf[msg.sender] -= _amount;
        deposits[msg.sender].deposits[0] = dep;
        return true;
    }

    function withdrawDeposit(uint number) returns (bool) {
        Deposit deposit = deposits[msg.sender].deposits[number];

        // tranfser fund and close deposit
        // now - current block time
        if (now - deposit.time >= deposit.withdrawTime && deposit.work) {
            deposit.work = false;
            balanceOf[msg.sender] += deposit.amount + deposit.amount/deposit.percent;
            deposit.amount = 0;
            deposits[msg.sender].deposits[0] = deposit;
            return true;
        }

        return false;
    }

    function depositBalance(uint number) constant returns (uint256, bool, uint, uint, uint) {
        Deposit deposit = deposits[msg.sender].deposits[number];

        if (now - deposit.time >= deposit.withdrawTime && deposit.work) {
            return (deposit.amount + deposit.amount/deposit.percent, false, deposit.percent, deposit.time, deposit.withdrawTime);
        }

        return (deposit.amount, deposit.work, deposit.percent, deposit.time, deposit.withdrawTime);
    }


    // getCoinBalance
    // @return balance

    // depositEther
    // @params _value

    // withdrawEther
    // @params _value

    // getFundBalance

    // makeDecision
    // @params string _description
    // @params uint256 _value

    // voteDecision
    // @params bool _answer

    // cancelDecision(ad) ...


}
